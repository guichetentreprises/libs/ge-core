/**
 * 
 */
package fr.ge.core.bean;

import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Full User Bean.
 * 
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public class GeUserBean extends UserBean {

  /**
   * Constructeur de la classe.
   *
   * @param userId
   *          : userId
   */
  public GeUserBean(final String userId) {
    super(userId);
  }

  /** The civility. */
  private String civility;

  /** The name. */
  private String name;

  /** The forName. */
  private String forName;

  /** The mail. */
  private String mail;

  /** The language. */
  private String language;

  /** The phone. */
  private String phone;

  /** The phone. */
  private boolean validPhone;

  /** The date connection. **/
  private Date connexionDate;

  /**
   * Accesseur sur l'attribut {@link #civility}.
   *
   * @return String civility
   */
  public String getCivility() {
    return this.civility;
  }

  /**
   * Mutateur sur l'attribut {@link #civility}.
   *
   * @param civility
   *          la nouvelle valeur de l'attribut civility
   */
  public void setCivility(final String civility) {
    this.civility = civility;
  }

  /**
   * Accesseur sur l'attribut {@link #name}.
   *
   * @return String name
   */
  public String getName() {
    return this.name;
  }

  /**
   * Mutateur sur l'attribut {@link #name}.
   *
   * @param name
   *          la nouvelle valeur de l'attribut name
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * Accesseur sur l'attribut {@link #forName}.
   *
   * @return String forName
   */
  public String getForName() {
    return this.forName;
  }

  /**
   * Mutateur sur l'attribut {@link #forName}.
   *
   * @param forName
   *          la nouvelle valeur de l'attribut forName
   */
  public void setForName(final String forName) {
    this.forName = forName;
  }

  /**
   * Accesseur sur l'attribut {@link #mail}.
   *
   * @return String mail
   */
  public String getMail() {
    return this.mail;
  }

  /**
   * Mutateur sur l'attribut {@link #mail}.
   *
   * @param mail
   *          la nouvelle valeur de l'attribut mail
   */
  public void setMail(final String mail) {
    this.mail = mail;
  }

  /**
   * Accesseur sur l'attribut {@link #language}.
   *
   * @return String language
   */
  public String getLanguage() {
    return this.language;
  }

  /**
   * Mutateur sur l'attribut {@link #language}.
   *
   * @param language
   *          la nouvelle valeur de l'attribut language
   */
  public void setLanguage(final String language) {
    this.language = language;
  }

  /**
   * Accesseur sur l'attribut {@link #phone}.
   *
   * @return String phone
   */
  public String getPhone() {
    return this.phone;
  }

  /**
   * Mutateur sur l'attribut {@link #phone}.
   *
   * @param phone
   *          la nouvelle valeur de l'attribut phone
   */
  public void setPhone(final String phone) {
    this.phone = phone;
  }

  /**
   * Accesseur sur l'attribut {@link #validPhone}.
   *
   * @return boolean validPhone
   */
  public boolean isValidPhone() {
    return this.validPhone;
  }

  /**
   * Accesseur sur l'attribut {@link #connexionDate}.
   *
   * @return Date connexionDate
   */
  public Date getConnexionDate() {
    return connexionDate;
  }

  /**
   * Mutateur sur l'attribut {@link #connexionDate}.
   *
   * @param connexionDate
   *          la nouvelle valeur de l'attribut connexionDate
   */
  public void setConnexionDate(final Date connexionDate) {
    this.connexionDate = connexionDate;
  }

  /**
   * Mutateur sur l'attribut {@link #validPhone}.
   *
   * @param validPhone
   *          la nouvelle valeur de l'attribut validPhone
   */
  public void setValidPhone(final boolean validPhone) {
    this.validPhone = validPhone;
  }

  @Override
  public int hashCode() {
    return HashCodeBuilder.reflectionHashCode(this);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    } else if (obj == null || getClass() != obj.getClass()) {
      return false;
    } else {
      return EqualsBuilder.reflectionEquals(this, obj);
    }
  }

}
