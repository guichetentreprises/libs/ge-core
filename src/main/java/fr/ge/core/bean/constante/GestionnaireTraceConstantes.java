package fr.ge.core.bean.constante;

/**
 * Interface pour le gestionnaire de trace.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public enum GestionnaireTraceConstantes {

  /** Message : Aucune information disponible: "N/D". */
  AUCUNE_INFO_DISPO("N/D"),

  /** Dossier. */
  KEY_DOSSIER("D"),

  /** Utilisateur. */
  KEY_UTILISATEUR("U"),

  /** Constante du logger fonctionnel. **/
  FONC("fonc"),

  /** Constante du logger technique. **/
  TECH("tech");

  /** La valeur de la constantes. */
  private String value;

  /**
   * Instancie un nouveau etat enum.
   *
   * @param value
   *          le value
   */
  GestionnaireTraceConstantes(final String value) {
    this.value = value;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return this.value;
  }

  /**
   * Getter de l'attribut value.
   * 
   * @return la valeur de value
   */
  public String getValue() {
    return this.value;
  }
}
