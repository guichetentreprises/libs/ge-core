/**
 * 
 */
package fr.ge.core.bean.constante;

/**
 * @author $Author: jzaire $
 * @version $Revision: 0 $
 */
public interface IResponseConstant {

  /** Response OK */
  String OK = "OK";

  /** Response KO */
  String KO = "KO";
}
