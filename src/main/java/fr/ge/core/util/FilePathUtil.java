/**
 * 
 */
package fr.ge.core.util;

import org.apache.commons.lang3.StringUtils;

import fr.ge.core.bean.constante.IFilePathConstante;

/**
 * Classe Utilitaire liée au Fichier System.
 * 
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
// TODO AOL 23-06-2017 : Supprimer ou déplacer cette classe !!
public final class FilePathUtil {

  /**
   * Constructeur de la classe.
   *
   */
  private FilePathUtil() {
    super();
  }

  /**
   * Methode pour générer le chemin absolu d'un repertoire ou d'un fichier. <br>
   * exemple : <br>
   * param : origin = file ou sftp <b> (voir {@link IFilePathConstante}) </b><br>
   * param : dir = ge_gent_dir_pj ou utilisateur_id ou ge_gent_dir_pj/utilisateur_id <br>
   * result : file://${ge_gent_dir_pj}/${utilisateur_id}/${numero_dossier}
   * 
   * @param originFile
   *          : origine du fichier (merci d'utiliser {@link IFilePathConstante})
   * @param elements
   *          : liste de répertoire et peut se finir par un fichier (une transformation supprime les
   *          éventuels / à la fin et au début
   * @return : le chemin absolu du repertoire ou du fichier
   */
  public static String generateAbsoluteDirPath(final String originFile, final String... elements) {

    StringBuilder builder = new StringBuilder();
    builder.append(originFile).append(":").append(IFilePathConstante.FILE_SEPARATOR);
    for (String element : elements) {
      // on supprime les / à la fin et au début pour éviter les triples /
      String elementWithoutSlash = StringUtils.removeStart(StringUtils.removeEnd(element, IFilePathConstante.FILE_SEPARATOR),
        IFilePathConstante.FILE_SEPARATOR);
      builder.append(IFilePathConstante.FILE_SEPARATOR);
      builder.append(elementWithoutSlash);
    }
    return builder.toString();
  }

}
