package fr.ge.core.exception;

import java.util.UUID;

/**
 * Classe mère pour l'ensemble des Exception Guichet-Entreprises.
 * 
 */
public class TechniqueRuntimeException extends RuntimeException {

  /** UUID pour sérialisation. */
  private static final long serialVersionUID = 9075682936783438061L;

  /** Code de l'erreur. */
  private String code;

  /** Identifiant unique de l'erreur. */
  private String uuid;

  /**
   * Constructeur de la classe.
   * 
   * @param message
   *          message de l'exception technique
   */
  public TechniqueRuntimeException(final String message) {
    super(message);
    this.uuid = UUID.randomUUID().toString();
  }

  /**
   * Constructeur de la classe.
   * 
   * @param message
   *          message
   * @param cause
   *          cause
   */
  public TechniqueRuntimeException(final String message, final Throwable cause) {
    super(message, cause);
    this.uuid = UUID.randomUUID().toString();
  }

  /**
   * Constructeur de la classe.
   * 
   * @param cause
   *          Cause mère de l'erreur
   */
  public TechniqueRuntimeException(final Throwable cause) {
    super(cause);
    this.uuid = UUID.randomUUID().toString();
  }

  /**
   * Accesseur sur l'attribut {@link #uuid}.
   * 
   * @return uuid
   */
  public String getUuid() {
    return uuid;
  }

  /**
   * Mutateur sur l'attribut {@link #uuid}.
   * 
   * @param uuid
   *          UUID
   */
  public void setUuid(final String uuid) {
    this.uuid = uuid;
  }

  /**
   * Accesseur sur l'attribut {@link #code}.
   * 
   * @return code Code de l'erreur
   */
  public String getCode() {
    return code;
  }

  /**
   * Mutateur sur l'attribut {@link #code}.
   * 
   * @param code
   *          Code de l'erreur
   */
  public void setCode(final String code) {
    this.code = code;
  }
}
