package fr.ge.core.exception;

import java.util.UUID;

/**
 * Classe mère pour l'ensemble des Exception Guichet-Entreprises.
 * 
 */
public class TechniqueException extends Exception {

  /** UUID pour sérialisation. */
  private static final long serialVersionUID = -8118427365022556070L;

  /** Code de l'erreur. */
  private String code;

  /** Identifiant unique de l'erreur. */
  private String uuid;

  /**
   * Constructeur de la classe.
   * 
   * @param message
   *          message de l'exception technique
   */
  public TechniqueException(final String message) {
    super(message);
    this.uuid = UUID.randomUUID().toString();
  }

  /**
   * Constructeur de la classe.
   * 
   * @param message
   *          message
   * @param cause
   *          cause
   */
  public TechniqueException(final String message, final Throwable cause) {
    super(message, cause);
    this.uuid = UUID.randomUUID().toString();
  }

  /**
   * Constructeur de la classe.
   * 
   * @param code
   *          code de l'exception technique
   * @param message
   *          message de l'exception technique
   */
  public TechniqueException(final String code, final String message) {
    super(message);
    this.code = code;
    this.uuid = UUID.randomUUID().toString();
  }

  /**
   * Constructeur de la classe.
   * 
   * @param code
   *          code de l'exception technique
   * @param message
   *          message
   * @param cause
   *          cause
   */
  public TechniqueException(final String code, final String message, final Throwable cause) {
    super(message, cause);
    this.uuid = UUID.randomUUID().toString();
    this.code = code;
  }

  /**
   * Constructeur de la classe.
   * 
   * @param cause
   *          Cause mère de l'erreur
   */
  public TechniqueException(final Throwable cause) {
    super(cause);
    this.uuid = UUID.randomUUID().toString();
  }

  /**
   * Accesseur sur l'attribut {@link #uuid}.
   * 
   * @return uuid
   */
  public String getUuid() {
    return uuid;
  }

  /**
   * Mutateur sur l'attribut {@link #uuid}.
   * 
   * @param uuid
   *          UUID
   */
  public void setUuid(final String uuid) {
    this.uuid = uuid;
  }

  /**
   * Accesseur sur l'attribut {@link #code}.
   * 
   * @return code Code de l'erreur
   */
  public String getCode() {
    return code;
  }

  /**
   * Mutateur sur l'attribut {@link #code}.
   * 
   * @param code
   *          Code de l'erreur
   */
  public void setCode(final String code) {
    this.code = code;
  }
}
