/**
 * 
 */
package fr.ge.core.message;

import java.util.Locale;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;

import fr.ge.core.log.GestionnaireTrace;

/**
 * Fournit des messages avec ou sans paramètre.
 *
 * @author $Author: amonsone $
 * @version $Revision: 0 $
 */
public abstract class AbstractMessageProvider {

  /** Messages traces. */
  @Autowired
  private MessageSource messagesTraces;

  /** La constante LOGGER_FONC. */
  private static final Logger LOGGER_TECH = GestionnaireTrace.getLoggerTechnique();

  /** Le msg introuvavble. */
  private static String msgIntrouvable = "Le code du message  n'est pas référencé dans le fichier des messages : ";

  /**
   * Récupère le message et applique les paramètres.
   *
   * @param code
   *          le code
   * @param params
   *          les params
   * @return le message
   */
  public String getMessage(final String code, final Object... params) {
    String message = null;
    try {
      message = messagesTraces.getMessage(code, params, Locale.getDefault());
    } catch (NoSuchMessageException e) {

      LOGGER_TECH.warn(msgIntrouvable + code, e.getCause());
      return null;
    }

    return message;
  }

}
