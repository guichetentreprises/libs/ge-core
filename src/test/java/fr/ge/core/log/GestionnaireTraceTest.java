/**
 * 
 */
package fr.ge.core.log;

import static org.fest.assertions.Assertions.assertThat;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import org.junit.Test;
import org.slf4j.MDC;

import fr.ge.core.bean.constante.GestionnaireTraceConstantes;

/**
 * Classe de test pour GestionnaireTrace.
 * 
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public class GestionnaireTraceTest {

  /**
   * Methode de test de l'attribut de la class.
   * 
   * @throws Exception
   *           : Exception
   */
  @Test
  public void classTest() throws Exception {
    final Constructor < ? > constructor = GestionnaireTrace.class.getDeclaredConstructor();
    assertThat(constructor.isAccessible()).isFalse();
    assertThat(Modifier.isPrivate(constructor.getModifiers())).isTrue();
    constructor.setAccessible(true);
    constructor.newInstance();
    assertThat(constructor.isAccessible()).isTrue();
    constructor.setAccessible(false);
  }

  /**
   * Methode de test pour getInstanceTest.
   */
  @Test
  public void getInstanceTest() {
    assertThat(GestionnaireTrace.getInstance()).isNotNull();
    assertThat(GestionnaireTrace.getLoggerFonctionnel()).isNotNull();
    assertThat(GestionnaireTrace.getLoggerTechnique()).isNotNull();
  }

  /**
   * Methode de test pour getInstanceTest.
   */
  @Test
  public void clearAllTest() {
    GestionnaireTrace.clearAll();
    assertThat(MDC.get(GestionnaireTraceConstantes.KEY_DOSSIER.getValue())).isNull();
    assertThat(MDC.get(GestionnaireTraceConstantes.KEY_UTILISATEUR.getValue())).isNull();
  }

}
